module gitlab.com/gonutz/mines

go 1.16

require (
	github.com/gonutz/check v1.2.0
	github.com/gonutz/mines v0.0.0-20190614184542-c2d35598a1fb
	github.com/gonutz/prototype v1.0.5
	github.com/gonutz/shuffle v1.0.0
)
